/**
 * Created by deniskashuba on 23.09.16.
 */

var http = require('http');
var util  = require('util'),
	spawn = require('child_process').spawn,
	ls    = spawn('heroku', ['logs', '--tail', '--ps', 'ho_queue', '--app', 'z360console-production']);
	//ls    = spawn('heroku', ['logs', '--tail', '--ps', 'ho_stat', '--app', 'z360console-production']);

var latestLog = 0;
var prevLatestLog = Date.now();

ls.stdout.on('data', function (data) {

	//console.log('stdout: ' + data.toString());

	latestLog = Date.now();

});

ls.stderr.on('data', function (data) {
	console.log('stderr: ' + data.toString());
	sendSms();
});

ls.on('exit', function (code) {
	console.log('child process exited with code ' + code.toString());
});


function checkStatus() {

	console.log(latestLog - prevLatestLog);

	if ((latestLog - prevLatestLog) < -10000 && (latestLog - prevLatestLog) > -60000) {

		console.log('logs stopped');
		sendSms();

	}

	prevLatestLog = Date.now();

	setTimeout(function() {

		checkStatus();

	}, 5000);

}

setTimeout(function() {

	checkStatus();

}, 10000);

function sendSms() {

	console.log('send sms');

	http.get({
		host: 'sms.ru',
		path: '/sms/send?api_id=6BFB6DA3-8944-9FD7-7849-986CF0DACD84&to=380688757905&text=problems+with+queue'
	}, function(response) {
		// Continuously update stream with data
		var body = '';
		response.on('data', function(d) {
			body += d;
		});
		response.on('end', function() {

			console.log(body);

		});
	});

	// Eugene
	http.get({
		host: 'sms.ru',
		path: '/sms/send?api_id=FDAEF31F-10FD-1BEB-4DFD-E47B0411CE21&to=380675731774&text=problems+with+queue'
	}, function(response) {
		// Continuously update stream with data
		var body = '';
		response.on('data', function(d) {
			body += d;
		});
		response.on('end', function() {

			console.log(body);

		});
	});

}